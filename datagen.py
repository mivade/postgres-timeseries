from argparse import ArgumentParser
from random import uniform, choice
import time
from datetime import datetime, timedelta, timezone
import sqlalchemy as sa


def make_table(engine):
    metadata = sa.MetaData()
    table = sa.Table(
        'timeseries', metadata,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('timestamp', sa.DateTime(timezone=True),
                  nullable=False, index=True),
        sa.Column('sensor', sa.String(length=128), nullable=False, index=True),
        sa.Column('temperature', sa.Float(precision=4), nullable=False))
    metadata.create_all(bind=engine)
    return table


def insert_rows(engine, table, N_rows, N_sensors):
    def dt(seconds):
        return timedelta(seconds=seconds, milliseconds=uniform(0, 999))

    sensors = ["sensor_%02d" % n for n in range(N_sensors)]
    now = datetime.now(timezone.utc)

    data = [dict(timestamp=(now - dt(seconds=(N_rows - i))),
                 sensor=choice(sensors),
                 temperature=uniform(18, 26)) for i in range(N_rows)]
    with engine.connect() as conn:
        ins = table.insert()
        conn.execute(ins, data)


def parse_args():
    parser = ArgumentParser(description="Generate random timeseries data")
    parser.add_argument("--url", type=str,
                        default="postgresql://test:test@localhost/test",
                        help="Database URL")
    parser.add_argument("--echo", action="store_true",
                        help="Echo SQL commands")
    parser.add_argument("sensors", type=int, help="Number of sensors")
    parser.add_argument("points", type=int, default=1e6, nargs="?",
                        help="Number of points to add")
    return parser.parse_args()


def main():
    args = parse_args()
    engine = sa.create_engine(args.url, echo=args.echo)
    table = make_table(engine)
    start = time.time()
    print("Inserting %d rows..." % args.points)
    insert_rows(engine, table, args.points, args.sensors)
    print("Finished in %.3f s" % (time.time() - start))
    drop = ''
    while True:
        drop = input("Drop table? [n]").strip().lower()
        if drop not in ('y', 'n', ''):
            print("Please answer y or n")
            continue
        else:
            if drop == 'y':
                print("Dropping table from database...")
                table.drop(bind=engine)
            break


if __name__ == "__main__":
    main()
