Sample code for using Postgres and Python for time series data.
[Associated blog post](https://mike.depalatis.net/using-postgres-as-a-time-series-database.html).
