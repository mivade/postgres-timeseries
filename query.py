from datetime import datetime, timezone
from sqlalchemy import create_engine, MetaData, Table
import pandas as pd

engine = create_engine('postgresql://test:test@localhost/test')
meta = MetaData(bind=engine)
table = Table("timeseries", meta, autoload=True)

today = datetime.now(timezone.utc)
today = today.replace(hour=0, minute=0, second=0, microsecond=0)


def raw_sql_example():
    """Example of reading from the timeseries table with a raw SQL
    query.

    """
    query = (
        "SELECT * FROM timeseries " +
        "WHERE timestamp >= '{}' ".format(today.isoformat()) +
        "AND sensor = 'sensor_01'"
    )
    print(query)
    df = pd.read_sql_query(query, engine, index_col="timestamp")
    print(df.head())


def sqlalchemy_example():
    """Same as the raw query but with SQLAlchemy core."""
    select = table.select(table).where(table.c.timestamp >= today)\
        .where(table.c.sensor == "sensor_01")
    print(select)
    df = pd.read_sql_query(select, engine, index_col="timestamp")
    print(df.head())


def aggregation_example():
    """Aggregate timeseries data server side."""
    query = (
        "SELECT " +
        "date_trunc('hour', timestamp) AS timestamp, " +
        "avg(temperature) AS temperature " +
        "FROM timeseries " +
        "WHERE timestamp >= '2016-06-25' AND sensor = 'sensor_01' " +
        "GROUP BY date_trunc('hour', timestamp);"
    )
    print(query)
    df = pd.read_sql_query(query, engine, index_col="timestamp")
    print(df.head())


if __name__ == "__main__":
    print("Raw SQL:\n")
    raw_sql_example()
    input("Press enter to continue...")

    print("\n\nSQLAlchemy core example:\n")
    sqlalchemy_example()

    input("Pres enter to continue...")
    print("\n\nAggregation example:\n")
    aggregation_example()
